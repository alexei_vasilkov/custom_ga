import matplotlib.pyplot as plt
import copy
import random
import numpy as np
from evo.dnainbits import *
import colored

def color_this(s, fg, bg=-1):
    if bg != -1:
        return '{0}{1}{2}{3}'.format(colored.fg(fg), colored.bg(bg), s, colored.attr(0))
    else:
        return '{0}{1}{2}'.format(colored.fg(fg), s, colored.attr(0))

RED = (196, 52)
GREEN = (41, 22)



#increase number of chromosomes!
#http://www.gene-expression-programming.com/GepBook/Introduction.htm

class GeneStructureObj:

    def __init__(self, floats_number, gene_type_size, regulatory_true_th, enabled_size, enabled_true_th, window_size_in_bits):
        self.floats_number = floats_number
        self.gene_type_size = gene_type_size
        self.enabled_size = enabled_size
        self.window_size_in_bits = window_size_in_bits#regulatory window in both directions
        self.enabled_true_th = enabled_true_th
        self.regulatory_true_th = regulatory_true_th

    def get_offset(self, gene):
        return gene*self.floats_number

class DNA(DNAinBits):

    def __init__(self, genes_number, gene_structure_obj):
        DNAinBits.__init__(self, genes_number*gene_structure_obj.floats_number)
        self.gene_obj = gene_structure_obj
        #chromosomes allow a lot of crossovers so just make a lot of them.
        self.randomize()

    @property
    def size_in_genes(self):
        return len(self.data)//self.gene_obj.floats_number

    def copy(self):
        return copy.deepcopy(self)

    def clip_size(self, max_dna_size_in_genes):
        if self.size_in_genes > max_dna_size_in_genes:
            self.data = self.data[:max_dna_size_in_genes]

    @staticmethod
    def many_point_crossover(dna1, dna2, points_n):
        """ WARNING copies dna objects """
        dna1 = dna1.copy()
        dna2 = dna2.copy()

        #WARNING min length is used for points generations
        max_length = min(dna1.size_in_bits, dna2.size_in_bits)
        points = sorted(np.random.random_integers(0, max_length, size=points_n))#inclusive
        for i in range(0, (len(points)//2)*2, 2):
            DNAinBits.swap_parts(dna1, dna2, points[i], points[i + 1])
        if points_n % 2 != 0:
            DNAinBits.swap_parts(dna1, dna2, points[-1], max_length)

        return dna1, dna2#, list(points)#uncomment for debug

    def get_gene_metadata(self):
        """ 0 enabled, 1 - type, 2 - window/id, 3 - enhancer """
        #PROBLEM regulatory genes here rgulate unrelated genes that happen to be around it and not some related group.
        # SIMPLE GENE IS 2
        types = [0]*self.size_in_genes
        regulatory_background = [1.0]*self.size_in_genes
        for i in range(self.size_in_genes):
            ii = self.gene_obj.get_offset(i)
            types[i] = self.get_bool_from_left(ii, self.gene_obj.enabled_size, self.gene_obj.enabled_true_th)
            if types[i] != 0:
                types[i] = 1 if self.get_bool_from_left(ii + 1, self.gene_obj.gene_type_size, self.gene_obj.regulatory_true_th) else 2
                if types[i] == 1:
                    window = self.get_raw_float(ii + 2, self.gene_obj.window_size_in_bits)
                    if window > 0:
                        #WARNING MAGIC CONSTANT OUT OF THE BLUE 0.2
                        enhancer = 0.2*(self.get_float(ii + 3)*2 - 1)#allows negative regulation! IMPORTANT
                        for k in range(max(0, i - window), min(self.size_in_genes, i + window + 1)):
                            """ access dictionary only once pre cycle TODO """
                            regulatory_background[k] += enhancer*(1 - abs(k - i)/window)#larger than one right now

        return types, regulatory_background

    def get_gene_values(self, template_genes, step, defaults=None):
        """ WARNING templates must be sorted """
        types, reg_bg = self.get_gene_metadata()
        n = len(template_genes)
        d = {i:0 for i in range(n)}
        d_marked = {i:False for i in range(n)}
        genes = []
        for i in range(self.size_in_genes):
            if types[i] == 2:
                ii = self.gene_obj.get_offset(i)
                genes.append((self.data[ii + 2], self.get_float(ii + 3)*reg_bg[i]))
                #print(self.get_float(ii + 3),reg_bg[i])

        genes.sort(key=lambda x: x[0])

        n_g = len(genes)

        i = 0
        j = 0
        while i < n_g and j < n:
            k = 0
            if genes[i][0] - template_genes[j] > step:
                j += 1
                continue
            else:
                while j + k < n and abs(template_genes[j + k] - genes[i][0]) < step:
                    d[j + k] += (1 - abs(template_genes[j + k] - genes[i][0])/step)*genes[i][1]
                    d_marked[j + k] = True
                    k += 1
                i += 1

        for k, v in d.items():#WARNING CLIPPING TO 1.0
            d[k] = min(d[k], 1.0)

        if defaults is not None:
            for k, v in d.items():
                if not d_marked[k]:
                    d[k] = defaults[k]
        else:
            for k, v in d.items():
                if not d_marked[k]:
                    d[k] = None


        return d

def colorize_crossover(dna1, dna2, points, c1, c2):
    s1 = dna1.get_bit_string()
    s2 = dna2.get_bit_string()

    s11 = ''
    s22 = ''
    points = [0] + points
    if len(points) % 2 != 0 or points[-1] != dna1.size_in_bits:
        points.append(dna1.size_in_bits)

    for i in range(len(points) - 1):
        s11 += color_this(s1[points[i]:points[i+1]], *c1)
        s22 += color_this(s2[points[i]:points[i+1]], *c2)
        c1, c2 = c2, c1
    return s11, s22

def colorize_insertion_copy(dna, src_st, dst_st, size, c1, c2):
    s = dna.get_bit_string()
    s = color_this(s[:src_st], *c1) + color_this(s[src_st:src_st+size], *c2) + color_this(s[src_st+size:dst_st], *c1) + color_this(s[dst_st:dst_st+size], *c2) + color_this(s[dst_st+size:], *c1)
    return s

"""
n = 300
step = 2**32/(n + 1)
random.seed(10)
template_genes = sorted(np.random.random_integers(0, 2**32, n))
template_genes_defaults = np.random.rand(n)
genes_n = 600
g_obj = GeneStructureObj(4, 32, 32, 3)
a = DNA(genes_n, g_obj)
#a.print()
#print(a.get_gene_metadata())
regulatory_background = a.get_gene_metadata()[1]
# SIMPLE GENE IS FALSE value type
import matplotlib.pyplot as plt
plt.plot(regulatory_background, 'r')
plt.show()
import time
wq12 = time.clock()
genes = a.get_gene_values(template_genes, step, template_genes_defaults)
print(time.clock() - wq12)
s = 0
for k, v in genes.items():
    if v <= 0.0001:
        #print(template_genes[k], v, buckets[int(template_genes[k]//step)])
        s += 1
print(s, 'out of', n, 'are zero')

genes_to_draw = list(list(zip(*sorted(genes.items(), key=lambda x: x[0])))[1])
plt.plot(template_genes, genes_to_draw, 'r', marker='o')
plt.show()
"""
"""
s = a.get_bit_string()
print(color_this(s, *GREEN))
st1 = 0
st2 = 32
size = 32
a.insertion_copy(st1, st2, size)
print(colorize_insertion_copy(a, st1, st2, size, GREEN, RED))
a.print()
a.swap_genes(0, 1)
a.print()
a.print()
a.genewise_insertion_copy(1, 0, 2)
a.print()
"""
"""

a1, b1, points = DNA.many_point_crossover(a, b, 1)

s11, s22 = colorize_crossover(a1, b1, points, GREEN, RED)
print(color_this(a.get_bit_string(), *GREEN))
print(s11)
print(s22)
print(color_this(b.get_bit_string(), *RED))

print('-'*32*3)
a1, b1, points = DNA.many_point_crossover(a, b, 2)

s11, s22 = colorize_crossover(a1, b1, points, GREEN, RED)
print(color_this(a.get_bit_string(), *GREEN))
print(s11)
print(s22)
print(color_this(b.get_bit_string(), *RED))

"""
