import matplotlib.pyplot as plt
import multiprocessing as mp
import random
import numpy as np
from evo.r import DNA, GeneStructureObj
def coin_flip_swap(first, second):
    if random.random() < 0.5:
        return second, first
    return first, second

def roulette_wheel(weights, max_weight):
    """ from https://en.wikipedia.org/wiki/Fitness_proportionate_selection
    chooses an index based on weight values as probabilities"""
    n = len(weights)
    limit = 5#check if it's enough
    while limit > 0:
        index = int(n*random.random())#random() in [0,1) and int(1.9)==1
        if random.random() < weights[index]/max_weight:
            break
        limit -= 1
    return index

def larger_better_comp(better, worse):
    return better > worse

def smaller_better_comp(better, worse):
    return better < worse


class MutationRates:

    def __init__(self, max_dna_size_in_genes, m_rate, crossover_rate, max_points_crossover, replacement_with_copy_rate, replacement_with_copy_max_size,
            genewise_insertion_rate, genewise_insertion_max_size, genewise_deletion_rate, genewise_deletion_max_size):
        self.m_rate = m_rate
        self.crossover_rate = crossover_rate
        self.max_points_crossover = max_points_crossover
        self.replacement_with_copy_rate = replacement_with_copy_rate
        self.replacement_with_copy_max_size = replacement_with_copy_max_size
        self.genewise_insertion_rate = genewise_insertion_rate
        self.genewise_insertion_max_size = genewise_insertion_max_size
        self.genewise_deletion_rate = genewise_deletion_rate
        self.genewise_deletion_max_size = genewise_deletion_max_size
        self.max_dna_size_in_genes = max_dna_size_in_genes


def generate_part(dna_size, max_size):
    return random.randrange(dna_size), random.randrange(dna_size), random.randrange(1, max_size)




def compute_multiple_fitnesses(orgs, fitness_f, out_q=None, pid=-1):
    res = [fitness_f(org) for org in orgs]

    if out_q is None:
        return res
    else:
        out_q.put((pid, res))

class EvoAlg:

    def __init__(self, orgs, pop_size, elite_partition, fitness_f, iterations, mutation_rates,
            termination_fitness=None, larger_better=True,
            add_random_organisms_per_iteration=0, in_parallel=0, plotting=False, save_best_every=None, save_best_f=None):
        self.save_best_every = save_best_every
        self.save_best_f = save_best_f
        self.orgs = orgs
        self.termination_fitness = termination_fitness
        self.add_random_organisms_per_iteration = add_random_organisms_per_iteration
        self.larger_better = larger_better
        self.pop_size = pop_size
        self.fitness_f = fitness_f
        self.iterations = iterations
        self.elite_partition = elite_partition
        self.plot_data = []#best org, avg fitness, max f
        self.plotting = plotting
        self.m_rates = mutation_rates
        self.in_parallel = in_parallel
        self.best = []

    def compute_fitnesses(self):
        if self.in_parallel > 1:
            p_n = self.in_parallel
            part_l = int(len(self.orgs)/p_n)
            processes = []
            out_q = mp.Queue()
            for i in range(p_n - 1):
                processes.append(mp.Process(target=compute_multiple_fitnesses,
                    args=(self.orgs[i*part_l : (i+1)*part_l], self.fitness_f, out_q, i)))
                processes[-1].start()

            compute_multiple_fitnesses(self.orgs[(p_n - 1)*part_l:], self.fitness_f, out_q, p_n - 1)

            for i in range(p_n - 1):
                processes[i].join()
            res = [None]*p_n
            for i in range(p_n):
                q = out_q.get()
                res[q[0]] = q[1]

            r = []
            for i in range(p_n):
                r += res[i]

            return list(zip(self.orgs, r))


        else:
            return list(zip(self.orgs, compute_multiple_fitnesses(self.orgs, self.fitness_f)))

    def get_elite(self, orgs_with_fit):
        best = sorted(orgs_with_fit, key=lambda x: x[1], reverse=self.larger_better)
        return [b[0] for b in best[:max(int(len(best)*self.elite_partition), 1)]]

    def mutate_organism(self, org):
        """ WARNING MODIFIES ORGANISM  VARIABLE"""
        org.mutate(self.m_rates.m_rate)

        if random.random() < self.m_rates.replacement_with_copy_rate:
            org.replacement_with_copy(*generate_part(org.size_in_bits, self.m_rates.replacement_with_copy_max_size))

        if random.random() < self.m_rates.genewise_insertion_rate:
            org.true_genewise_insertion(*generate_part(org.size_in_genes, self.m_rates.genewise_insertion_max_size), org.gene_obj.floats_number)

        if random.random() < self.m_rates.genewise_deletion_rate:
            org.true_genewise_deletion(*generate_part(org.size_in_genes, self.m_rates.genewise_insertion_max_size)[1:], org.gene_obj.floats_number)
        #TODO make swap and others work with different sizes maybe like real dna fund similar parts or 2 strands and crossover between the same size strands

        org.clip_size(self.m_rates.max_dna_size_in_genes)
        return org

    def sexual_reproduction(self, org1, org2):
        org = coin_flip_swap(*DNA.many_point_crossover(org1, org2, random.randint(1, self.m_rates.max_points_crossover)))[0]
        return self.mutate_organism(org)

    def next_gen(self, orgs_with_fit, max_fitness):
        elite = self.get_elite(orgs_with_fit)
        orgs, fitnesses = zip(*orgs_with_fit)#inverse order from actual

        if int(self.pop_size*self.elite_partition) > 0:
            next_gen = elite[:]
        else:
            next_gen = []
        if self.add_random_organisms_per_iteration > 0:
            add_random = random.randrange(self.add_random_organisms_per_iteration) + 1
            for i in range(add_random):
                next_gen.append(DNA(self.orgs[0].size_in_genes, self.orgs[0].gene_obj))

        fitnesses = list(fitnesses)
        if not self.larger_better:
            for i in range(len(fitnesses)):
                fitnesses[i] = -fitnesses[i]

        #in case best fitness is zero and need positives for stochastic acceptance
        f_min = min(fitnesses)
        for i in range(len(fitnesses)):
            fitnesses[i] -= f_min - 0.1


        max_fitness -= f_min - 0.1

        i = 0
        nnn = self.pop_size - len(next_gen)
        while i < nnn:
            p1 = roulette_wheel(fitnesses, max_fitness)
            p2 = roulette_wheel(fitnesses, max_fitness)
            if random.random() < self.m_rates.crossover_rate:
                k1 = len(orgs[p1].data)
                k2 = len(orgs[p2].data)
                if k1/k2 < 0.5 or k2/k1 > 2.0:
                    continue
                next_gen.append(self.sexual_reproduction(orgs[p1], orgs[p2]))
            else:
                next_gen.append(self.mutate_organism(orgs[p1].copy()))#important. copy()
            if len(next_gen[-1].data) == 0:
                del next_gen[-1]
                continue
            i += 1

        self.orgs = next_gen
        return elite[0]

    def run(self):
        self.best = []
        comp = larger_better_comp if self.larger_better else smaller_better_comp
        max_fit = None
        if self.plotting:
            plt.ion()
            graph_obj = (plt.plot([], [], 'g')[0], plt.plot([], [], 'r')[0])

        for i in range(self.iterations):
            orgs_with_fit = self.compute_fitnesses()
            max_fitness = orgs_with_fit[0][1]
            avg = 0
            for o in orgs_with_fit:
                if comp(o[1], max_fitness):
                    max_fitness = o[1]
                avg += o[1]
            avg /= len(orgs_with_fit)


            if self.plotting:
                self.plot_data.append((min(avg, 300000000), min(max_fitness, 300000000)))


            best = self.next_gen(orgs_with_fit, max_fitness).copy()
            self.best.append((best, max_fitness))


            print('----------------')
            print("Iteration ", i)
            print("Max: %0.5f" % max_fitness)
            print('DNA size: ', self.best[-1][0].size_in_genes)
            print("Average: %0.5f" % avg)
            if max_fit is None:
                max_fit = max_fitness
            if max_fit < max_fitness:
                max_fit = max_fitness
            if max_fit < avg:
                max_fit = avg
            if self.plotting:

                #genes = list(zip(*sorted(self.best[-1][0].get_gene_values(template_genes, step, template_genes_defaults).items(), key=lambda x:x[0])))[1]
                #print(template_genes_defaults)
                #print(template_genes, [self.best[-1][0].data[_] for _ in range(2, len(self.best[-1][0].data), 4)])
                #print(genes, self.best[-1][0].size_in_genes)
                avgy, maxy = zip(*self.plot_data)
                plt.gca().set_xlim((0,i+1))
                plt.gca().set_ylim((0,min(max_fit*1.05, 3000000)))
                plt.gca().set_ylabel('%0.5f' % max_fitness)
                graph_obj[0].set_data(np.arange(i+1), avgy)
                graph_obj[1].set_data(np.arange(i+1), maxy)
                plt.draw()
                plt.pause(0.0001)
            if self.save_best_every is not None and (i + 1) % self.save_best_every == 0:
                self.save_best_f(self.best[-1], i)


            if self.termination_fitness is not None and comp(max_fitness, self.termination_fitness) or self.termination_fitness == max_fitness:
                #print(max_fitness)
                #print('booo')
                #print(self.best[-1][0])
                if self.plotting:
                    plt.ioff()
                return self.best[-1][0]


"""
import matplotlib.pyplot as plt
pop_size = 30
template_genes_number = 10
step = 2**32/(template_genes_number + 1)
template_genes = sorted(np.random.random_integers(0, 2**32, template_genes_number))
template_genes_defaults = np.random.rand(template_genes_number)
pattern010101 = np.array([int(i % 2 != 0) for i in range(template_genes_number)])

#test it with 01010101 pattern not 11111 and fix solve different size by double strandedness. but first test it with 010101
def f_f(dna):
    genes = dna.get_gene_values(template_genes, step, template_genes_defaults).values()
    return sum(genes)

def f_f2(dna):
    genes = list(zip(*sorted(dna.get_gene_values(template_genes, step, template_genes_defaults).items(), key=lambda x:x[0])))[1]
    return sum(map(abs, pattern010101 - genes))


dna_size_in_genes = 16
max_dna_size_in_genes = 50
m_rates = MutationRates(max_dna_size_in_genes, 0.01, 0.8, 5, 0.05, 640, 0.001, 3, 0.001, 2)
g_obj = GeneStructureObj(4, 32, 32, 3)
orgs = [DNA(dna_size_in_genes, g_obj) for _ in range(pop_size)]
ea = EvoAlg(orgs, pop_size, 0.1, f_f2, 1000, m_rates, template_genes_number*0.999, True, int(pop_size*0.01), 4, True)
ea.run()
"""

