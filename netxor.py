from alg import *
from network import *
import random
import matplotlib.pyplot as plt

random.seed(10)
np.random.seed(10)

#network params
weights_scaler = 10
input_dim = 2
output_dim = 1
hidden_dim = 3
output_ranges = [(0, 1)]

#GA params
pop_size = 50
template_genes_number = genes_per_neuron(input_dim, output_dim, hidden_dim)*(hidden_dim + output_dim)
step = 2**32/(template_genes_number + 1)
template_genes = sorted(np.random.random_integers(0, 2**32, template_genes_number))
print(template_genes)

dna_size_in_genes = template_genes_number
max_dna_size_in_genes = dna_size_in_genes*20
#template_genes_defaults = np.random.rand(template_genes_number)#careful can prevent from evolving?.. NO! because will be replaced only if vaule is Not set by DNA
#template_genes_defaults = [0]*template_genes_number

samples = [((0, 0), 0), ((1, 0), 1), ((1, 1), 0), ((0, 1), 1)]
samples_number = 20
def f_f_net(dna):
    genes = list(list(zip(*sorted(dna.get_gene_values(template_genes, step).items(), key=lambda x:x[0])))[1])
    #print(genes)
    net = construct_network(genes, weights_scaler, input_dim, output_dim, hidden_dim, output_ranges)
    ticks_needed = get_min_ticks(net)
    if ticks_needed == -1:
        #no meaning in testing return worst error possible
        ticks_needed = 0
    ticks_needed += 1#needed.
    error = 0
    for s in samples:
    #for i in range(samples_number):
        #s = random.choice(samples)
        net.feed_input_vector(s[0])
        for i in range(ticks_needed):
            net.tick()
        ans = net.read_output_vector()
        error += np.sum(np.square(s[1] - ans))
        net.reinit()
    error /= 4#samples_number
    return error



m_rates = MutationRates(max_dna_size_in_genes, 0.01, 0.8, 5, 0.05, 640, 0.01, 3, 0.01, 2)
g_obj = GeneStructureObj(4, 32, 0.7, 32, 0.4, 3)#TODO ADD type and boolean percents not just 50/50
orgs = [DNA(dna_size_in_genes, g_obj) for _ in range(pop_size)]
ea = EvoAlg(orgs, pop_size, 0.1, f_f_net, 100, m_rates, 0.02, False, int(pop_size*0.1), 4, True)
ea.run()
o = ea.best[-1][0]

genes = list(list(zip(*sorted(o.get_gene_values(template_genes, step).items(), key=lambda x:x[0])))[1])
net = construct_network(genes, weights_scaler, input_dim, output_dim, hidden_dim, output_ranges)
error = 0
#print(get_min_ticks(net))
ticks_needed = get_min_ticks(net)
if ticks_needed == -1:
    print('Inputs are unreachable from outputs')
ticks_needed += 1#needed.
for s in samples:
    net.feed_input_vector(s[0])
    for i in range(ticks_needed):
        net.tick()
    ans = net.read_output_vector()
    print(ans)
    print(s[1])
    error += np.sum(np.square(s[1] - ans))
    net.reinit()

print(genes)

