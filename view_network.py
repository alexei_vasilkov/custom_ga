import networkx as nx
import matplotlib.pyplot as plt
import numpy as np


def costruct_graph_from_net(net):
    G = nx.Graph()
    G.add_nodes_from(np.arange(net.input_dim + net.hidden_dim + net.output_dim + 1))
    for n in net.neurons:
        for nn, w in n.incoming_connections:
            G.add_edge(nn.id, n.id, weight=w)
    widths = []
    for e in G.edges():
        s = G.edge[e[0]][e[1]]['weight']
        widths.append(s*2)
    return G, widths

def display_network(G, widths):
    pos = nx.spring_layout(G)
    plt.figure(figsize=(8,8))
    nx.draw_networkx_edges(G, pos, alpha=0.3, width=widths, edge_color='m')
    #nodesize=[wins[v]*50 for v in H]
    #nx.draw_networkx_nodes(H,pos,node_size=nodesize,node_color='w',alpha=0.4)
    nx.draw_networkx_nodes(G,pos,node_size=1000,node_color='w',alpha=0.4)
    nx.draw_networkx_labels(G,pos,fontsize=14)
    plt.axis('off')
    plt.show()
"""
weights_scaler = 10
input_dim = 2
output_dim = 1
hidden_dim = 1
output_ranges = [(0, 1)]
genes = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7 ,0.8, 0.9, 1.0]
net = construct_network(genes, weights_scaler, input_dim, output_dim, hidden_dim, output_ranges)
graph, widths = costruct_graph_from_net(net)
display_network(graph, widths)
"""
