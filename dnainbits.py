from ctypes import c_uint32 as uint32
import numpy as np
import random
import shutil

MAX_INT = 4294967295.0
MAX_INT_BITS = 0xffffffff

def get_bits(x):
    return '{0:{fill}32b}'.format(x, fill='0')

def print_bits(x):
    print(get_bits(x))

def get_mask(s, e):
    return (MAX_INT_BITS>>(s+32-e))<<(32-e)

def swap_bitwise(a, b, start, end):
    c = get_mask(start, end)
    a1 = ((b&c)|(a&(~c)))
    b1 = ((a&c)|(b&(~c)))
    return a1, b1

def to_gray(x):
    return x ^ (x >> 1)

def from_gray(x):
    x ^= x>>16
    x ^= x>>8
    x ^= x>>4
    x ^= x>>2
    x ^= x>>1
    return x


class DNAinBits:

    def __init__(self, numbers_count):
        self.data = np.array([0]*numbers_count, dtype=np.uint32)
        #ADD energy need depending on DNA size!!! energy/mass for a child!! TODO

    @property
    def size_in_numbers(self):
        return len(self.data)

    @property
    def size_in_bits(self):
        return len(self.data)*32

    def randomize(self):
        for i in range(self.size_in_numbers):
            self.data[i] = int(round(MAX_INT*random.random()))

    def __getitem__(self, i):
        return self.data[i]

    def __setitem__(self, i, value):
        self.data[i] = value

    def get_float(self, i):
        return from_gray(self.data[i])/MAX_INT

    def get_raw_float(self, i, size_from_right):
        return self.data[i] & ((1 << size_from_right) - 1)

    def set_float(self, i, value):
        self.data[i] = to_gray(int(round(value*MAX_INT)))

    def get_bool_from_right(self, i, size, true_treshold):
        """ paste code from c++ here that is more efficient """
        """ size from right to left """
        s = 0
        for j in range(size):
            s += self.get_bit_in_number(i, 31 - j)

        return s > size*true_treshold

    def get_bool_from_left(self, i, size, true_treshold):
        """ paste code from c++ here that is more efficient """
        """ size from right to left """
        s = 0
        for j in range(size):
            s += self.get_bit_in_number(i, j)

        return s > size*true_treshold


    #def set_bool(self, i, value):
    #    self.data[i] = to_gray(int(round(value*MAX_INT)))

    def get_bit(self, bit_id):
        """ bit from left to right """
        return (self.data[bit_id//32]>>(31 - bit_id % 32)) & 1

    def get_bit_in_number(self, n_id, bit):
        """ bit from left to right """
        return (self.data[n_id]>>(31 - bit)) & 1

    def set_bit(self, bit_id, value):
        """ bit from left to right """
        if value == 1:
            self.data[bit_id//32] |= 1<<(31 - bit_id % 32)
        else:
            self.data[bit_id//32] &= ~(1<<(31 - bit_id % 32))

    def flip_bit(self, bit_id):
        """ bit from left to right """
        self.data[bit_id//32] ^= 1 << (31 - bit_id % 32)

    def mutate(self, m_rate):
        for i in range(self.size_in_bits):
            if random.random() < m_rate:
                self.flip_bit(i)

    def get_bit_string(self):
        s = ''
        for n in self.data:
            s += '{0:{fill}32b}'.format(n, fill='0')
        return s

    def print(self):
        s1 = ''
        s2 = ''

        for i in range(self.size_in_numbers):
            s = str(self.data[i])
            s1 += ' '*(32 - len(s)) + s + '|'
            s2 += '{0:{fill}32b}'.format(self.data[i], fill='0') + '|'

        tsize = shutil.get_terminal_size((80, 20))
        print('-'*tsize[0])
        n = len(s1)//tsize[0]

        for i in range(n):
            print(s1[i*tsize[0]:(i+1)*tsize[0]])
            print(s2[i*tsize[0]:(i+1)*tsize[0]])
            print('-'*tsize[0])
        if len(s1) % tsize[0] != 0:
            print(s1[n*tsize[0]:])
            print(s2[n*tsize[0]:])
            print('-'*tsize[0])

    def replacement_with_copy(self, src_start, dst_start, size):
        """ in bits """
        src_end = src_start + size
        dst_end = dst_start + size
        src_s = ''.join([get_bits(n) for n in self.data[src_start//32:src_end//32+1]])
        dst_s = ''.join([get_bits(n) for n in self.data[dst_start//32:dst_end//32+1]])
        dst_start_number = dst_start//32
        src_start %= 32
        dst_start %= 32
        dst_s = dst_s[:dst_start] + src_s[src_start:src_start+size] + dst_s[dst_start+size:]
        for i in range(min(len(dst_s)//32, len(self.data) - dst_start_number)):
            self.data[dst_start_number + i] = uint32(int(dst_s[i*32:(i+1)*32], 2)).value

    @staticmethod
    def swap_parts(dna1, dna2, start, end):
        if start >= dna1.size_in_bits or start >= end:
            return
        short_dna_offset = None
        if len(dna1.data) == len(dna2.data):
            dna1 = dna1.data
            dna2 = dna2.data
        else:
            if len(dna1.data) < len(dna2.data):
                dna1, dna2 = dna2, dna1
            _dna1 = dna1
            _dna2 = dna2
            dna2 = dna2.data
            short_dna_offset = random.randrange(len(dna1.data) - len(dna2))
            dna1 = dna1.data[short_dna_offset:short_dna_offset + len(dna2)]
            if random.random() < 0.5:
                _dna1, _dna2 = _dna2, _dna1
                dna1, dna2 = dna2, dna1


        n_start = start//32
        n_end = end//32
        dna1[n_start+1:n_end], dna2[n_start+1:n_end] = dna2[n_start+1:n_end].copy(), dna1[n_start+1:n_end].copy()
        if n_start != n_end:
            if end % 32 != 0:
                dna1[n_end], dna2[n_end] = swap_bitwise(dna1[n_end], dna2[n_end], 0, end % 32)
            dna1[n_start], dna2[n_start] = swap_bitwise(dna1[n_start], dna2[n_start], start % 32, 32)
        else:
            if end % 32 != 0:
                dna1[n_start], dna2[n_start] = swap_bitwise(dna1[n_start], dna2[n_start], start % 32, end % 32)
            else:
                dna1[n_start], dna2[n_start] = swap_bitwise(dna1[n_start], dna2[n_start], start % 32, 32)

        if short_dna_offset is not None:
            if len(_dna1.data) < len(_dna2.data):
                dna1, dna2 = dna2, dna1
                _dna1, _dna2 = _dna2, _dna1

            _dna1.data[short_dna_offset:short_dna_offset + len(dna2)] = dna1


    def swap_number_slices(self, g_pos1, g_pos2, size):
        p1 = g_pos1*size
        p2 = g_pos2*size
        self.data[p1:p1+size], self.data[p2:p2+size] = self.data[p2:p2+size].copy(), self.data[p1:p1+size].copy()

    def true_genewise_insertion(self, src_gene, dst_gene, size, gene_size):
        """ positions in genes not bits """
        sp1 = src_gene*gene_size
        sp2 = (src_gene + size)*gene_size
        dp1 = dst_gene*gene_size
        self.data = np.concatenate((self.data[:dp1], self.data[sp1:sp2], self.data[dp1:]))

    def copy(self):
        return copy.deepcopy(self)

    def true_genewise_deletion(self, start_gene, size, gene_size):
        """ positions in genes not bits """
        p1 = start_gene*gene_size
        p2 = (start_gene + size)*gene_size
        self.data = np.concatenate((self.data[:p1], self.data[p2:]))

    def genewise_replace_with_copy(self, src_pos, dst_pos, size, number_size):
        size *= number_size
        src_pos *= number_size
        dst_pos *= number_size

        self.data[dst_pos:dst_pos + size] = self.data[src_pos:src_pos + size].copy()
