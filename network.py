import numpy as np
from collections import deque

def sig(x):
    return 1.0 / (1.0 + np.exp(-x))

class NeuronDummy:

    def __init__(self, idd):
        self.value = 0
        self.id = idd

    def fire(self):
        pass

def from_out_to_int_path_len(net, i, t):
    q = deque([i + t])
    visited = [-1]*(len(net.neurons) + t)
    visited[i + t] = 0
    visited[net.input_dim] = -2#bias
    while q:
        idd = q.popleft()
        path_l = visited[idd] + 1
        for n, w in net.neurons[idd - t].incoming_connections:
            if visited[n.id] != -1:
                continue
            if n.id < net.input_dim:
                return path_l
            visited[n.id] = path_l
            q.append(n.id)
    return None

def get_min_ticks(net):
    t = len(net.stationary_neurons)
    max_path = -1
    for i in range(net.output_dim):
        p = from_out_to_int_path_len(net, i, t)
        if p is not None and p > max_path:
            max_path = p
    return max_path


class Neuron:

    def __init__(self, idd):
        self.id = idd#just an index in neurons list, not a true id + stationary neurons list length
        self.value = 0
        self.incoming_connections = []
        self.incoming_signal = 0
        self.activation = sig

    def reinit(self):
        self.incoming_signal = 0

    def process_incoming_signals(self):
        self.incoming_signal = 0
        for n, w in self.incoming_connections:
            self.incoming_signal += n.value*w

    def fire(self):
        self.value = self.activation(self.incoming_signal)


class Network:

    def __init__(self, input_dim, output_dim, hidden_dim, output_ranges):
        self.stationary_neurons = [NeuronDummy(_) for _ in range(input_dim + 1)]
        self.stationary_neurons[-1].value = 1#bias
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.hidden_dim = hidden_dim
        self.output_scaling = np.array([(s, e - s) for s, e in output_ranges])
        #outputs come first
        t = len(self.stationary_neurons)
        self.neurons = [Neuron(_ + t) for _ in range(self.hidden_dim + self.output_dim)]

    def reinit(self):
        for n in self.neurons:
            n.reinit()

    def tick(self):

        for n in self.neurons:
            n.process_incoming_signals()

        for n in self.neurons:
            n.fire()

    def feed_input_vector(self, input_vector):
        for i in range(self.input_dim):
            self.stationary_neurons[i].value = input_vector[i]

    def read_output_vector(self):
        output = np.zeros(self.output_dim)
        for i in range(self.output_dim):
            q = self.output_scaling[i]
            output[i] = self.neurons[i].value*q[1] + q[0]
        return output

    def process_input(self, input_vector, ticks_count):
        self.feed_input_vector(input_vector)
        for i in range(ticks_count):
            self.tick()
        return self.read_output_vector()

def genes_per_neuron(input_dim, output_dim, hidden_dim):
    return input_dim + hidden_dim + output_dim + 1#bias adds 1 each

def construct_network(gene_values, weights_scaler, input_dim, output_dim, hidden_dim, output_ranges):
    genes = [None]*len(gene_values)
    for i in range(len(gene_values)):
        if gene_values[i] is not None:
            genes[i] = gene_values[i]*2*weights_scaler - weights_scaler
    gene_values = genes
    net = Network(input_dim, output_dim, hidden_dim, output_ranges)
    k = hidden_dim + output_dim
    k2 = genes_per_neuron(input_dim, output_dim, hidden_dim)
    for i in range(k):

        for j in range(k):
            if gene_values[i*k2 + j] is not None:
                net.neurons[i].incoming_connections.append((net.neurons[j], gene_values[i*k2 + j]))

        for j in range(input_dim + 1):
            if gene_values[i*k2 + k + j] is not None:
                net.neurons[i].incoming_connections.append((net.stationary_neurons[j], gene_values[i*k2 + k + j]))

    return net

