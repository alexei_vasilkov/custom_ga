import matplotlib.pyplot as plt
import random
import numpy as np
def get_bits(x):
    return '{0:{fill}32b}'.format(x, fill='0')


n = 1000
#gather from both strands. max 0.75 from each. they have different enhancer values. dominance is just a boolean with bits counting. regulatory genes influence genes in their local proximity only I guess?
#or maybe set up systems... with presimulation template_genes to influence..?
step = 2**32/(n + 1)
random.seed(10)
template_genes = sorted(np.random.random_integers(0, 2**32, n))
n_g = n
genes = sorted(np.random.random_integers(0, 2**32, n_g))
genes2 = sorted(np.random.random_integers(0, 2**32, n_g))

def get_genes_influence(template_genes, genes):
    n = len(template_genes)
    n_g = len(genes)
    d = {i:0 for i in range(n)}
    i = 0
    j = 0
    while i < n_g and j < n:
        k = 0
        if genes[i] - template_genes[j] > step:
            j += 1
            continue
        else:
            while j + k < n and abs(template_genes[j + k] - genes[i]) < step:
                d[j] += (1 - abs(template_genes[j + k] - genes[i])/step)*0.5
                k += 1
            i += 1
    return d

import time
a = time.clock()
d = get_genes_influence(template_genes, genes)
d2 = get_genes_influence(template_genes, genes2)
for k, v in d2.items():
    d[k] = (d[k] + v)*0.5
print(time.clock() - a)

"""

#add introns? just mutate them and use fow waps and that's it. just unused genes in different places
buckets = [[] for i in range(n+1)]
for i in range(n_g):
    buckets[int(genes[i]//step)].append(genes[i])
a = time.clock()
q = 0
for i in range(n):
    w = int(template_genes[i]//step)
    for g in buckets[w]:
        d[i] += max(0, 1 - abs(template_genes[i] - g)/step)*0.5#multiply by enhancer..
        q += 1
    #if w - 1 > 0:
    #    for g in buckets[w-1]:
    #        d[i] += max(0, 1 - abs(template_genes[i] - g)/step)*0.5
    #if w + 1 < n+1:
    #    for g in buckets[w+1]:
    #        d[i] += max(0, 1 - abs(template_genes[i] - g)/step)*0.5
print(time.clock() - a)
print(q)
"""
s = 0
for k, v in d.items():
    if v <= 0.0001:
        #print(template_genes[k], v, buckets[int(template_genes[k]//step)])
        s += 1
print(s, 'out of', n, 'are zero')
plt.plot(template_genes, list(map(lambda x: max(0.05, min(1, x)), list(zip(*sorted(d.items(), key=lambda x: x[0])))[1])), 'r', marker='o')


"""
plt.subplot(2, 1, 1)
plt.hist(results[0], 50, facecolor='green', alpha=0.75)
plt.grid(True)
plt.subplot(2, 1, 2)
plt.hist(results[1], 50, facecolor='green', alpha=0.75)
plt.grid(True)
"""

plt.show()
