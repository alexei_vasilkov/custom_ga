from alg import *
import matplotlib.pyplot as plt
pop_size = 50
template_genes_number = 2
step = 2**32/(template_genes_number + 1)
template_genes = sorted(np.random.random_integers(0, 2**32, template_genes_number))
template_genes_defaults = np.random.rand(template_genes_number)#careful can prevent from evolving?.. NO! because will be replaced only if vaule is Not set by DNA
#template_genes_defaults = [0]*template_genes_number
pattern010101 = np.array([int(i % 2 != 0) for i in range(template_genes_number)])

#test it with 01010101 pattern not 11111 and fix solve different size by double strandedness. but first test it with 010101
#def f_f(dna):
#    genes = dna.get_gene_values(template_genes, step, template_genes_defaults).values()
#    return sum(genes)

def to_range(x, abs_max):
    return x*2*abs_max - abs_max

def f_f_F1(dna):
    genes = list(zip(*sorted(dna.get_gene_values(template_genes, step, template_genes_defaults).items(), key=lambda x:x[0])))[1]
    return (genes[0]*2*5.12 - 5.12)**2 + (genes[1]*2*5.12 - 5.12)**2

def f_f_F2(dna):
    genes = list(zip(*sorted(dna.get_gene_values(template_genes, step, template_genes_defaults).items(), key=lambda x:x[0])))[1]
    x1 = to_range(genes[0], 2048)
    x2 = to_range(genes[1], 2048)
    return 100*(x1**2 - x2)**2 + (1 - x1)**2

dna_size_in_genes = 3
max_dna_size_in_genes = 20
m_rates = MutationRates(max_dna_size_in_genes, 0.01, 0.8, 5, 0.05, 640, 0.1, 3, 0.01, 2)
g_obj = GeneStructureObj(4, 32, 0.9, 32, 0.5, 3)#TODO ADD type and boolean percents not just 50/50
orgs = [DNA(dna_size_in_genes, g_obj) for _ in range(pop_size)]
ea = EvoAlg(orgs, pop_size, 2/50, f_f_F2, 1000, m_rates, None, False, int(pop_size*0.1), 4, True)
ea.run()
